const bp = require('body-parser')
const express = require('express')
const { createPublisher } = require("@telecom-argentina/pepa-event-lib");
require('dotenv').config()
const app = express()
const port = 4003

var AWS = require('aws-sdk');

AWS.config.update({ region:  process.env.REGION });

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

app.post('/send', async (req, res) => {
	const uri = process.env.HOOK_URL;
	const eventName = 'transaction-approved';
	
	const body = req.body;
 	
	const publisher = await initializePublisher(uri)
  await publisher.publish(eventName, body, {bodyVersion: '1.0.0'});
	
	res.status(200).json({result: 'ok', body});
})

app.post('/level-service/reassign-level', async (req, res) => {
	const uri = process.env.LEVEL_PUBLISHER;
	const eventName = 'ppay.ltv.level-engine.1.user-level.reassigned';
	
	const body = req.body;

 	const publisher = await initializePublisher(uri)
  await publisher.publish(eventName, body, {bodyVersion: '1.0.0'});

	res.status(200).json({result: 'ok', body});
})

app.post('/level-service/assign-level', async (req, res) => {
	const uri = process.env.LEVEL_PUBLISHER;
	const eventName = 'ppay.ltv.level-engine.1.user-level.assigned';
	
	const body = req.body;

 	const publisher = await initializePublisher(uri)
  await publisher.publish(eventName, body, {bodyVersion: '1.0.0'});

	res.status(200).json({result: 'ok', body});
})

app.post('/financial-service/cashback/create', async (req, res) => {
	const uri = process.env.FINANCIAL_SERVICE_PUBLISHER;
	const eventName = 'ppay.engagement.promotion-service.checked-promotion.approved';

	const body = req.body;

	const publisher = await initializePublisher(uri)
	await publisher.publish(eventName, body, {bodyVersion: '1.0.0'});

	res.status(200).json({result: 'ok', body});
})

app.listen(port, () => {
	console.log(`Example app listening on port ${port}`)
})

async function initializePublisher(uri) {
	const publisher = createPublisher(uri);
  await publisher.initialize();
  console.log('Publisher initialized');

	return publisher
}