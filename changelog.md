# Changelog

Todos los cambios significativos sobre el proyecto se deben agregar en este archivo. El formato esta basado en
Keep a Changelog y este proyecto maneja Semantic Versioning.


# [1.0.1] - 2023-04-17

### Feature

-   Se actualiza libreria "@telecom-argentina/pepa-event-lib"
-   Se agregan endpoint para probar eventos de asignacion y reasignacion de niveles

# [1.0.0] - 202X-XX-XX

### Feature

-   Proyecto Base.

